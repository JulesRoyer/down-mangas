"use strict";
var casper = require('casper').create();
var fs = require('fs');

function downImage(url, chapitre){
	casper.wait(200, function(){
		casper.thenOpen(url, function(){
			var quoi = casper.evaluate(function(){
				return document.getElementById("image").attributes[1].value;
			});
			var img = quoi.split("/");
			var x = fs.absolute(oussa + "/" + nomDossier + "/" + chapitre + "/" + img[img.length-1]);
			casper.echo("Telechargement dans " + x);
			casper.then(function() {casper.download(quoi, x);});
		});
	});
}

// Parsing des arguments
var NOM = casper.cli.get("nom");
var DEPART = casper.cli.get("depart");
var ARRIVEE = casper.cli.get("arrivee");
var VOLUME = casper.cli.has("volume");
var PAGEMAX = 0;
var quoi = " ";
var chap = 0;
var nomDossier = " ";
var oussa = casper.cli.get("ou");

//Demarrage
casper.echo("\n\n### Démarrage\n## Accès à japscan, contournement de la protection CloudFlare...");
casper.start();
casper.thenOpen("https://www.japscan.to");

// Tada
casper.wait(6000, function(){
	casper.echo("## Protection CloudFlare contournée !");
	casper.thenOpen("https://www.japscan.to/lecture-en-ligne/" + NOM + "/", function(){
		casper.waitForSelector(".clearfix", function(){
			nomDossier = casper.getHTML(".container .clearfix p").split(": ")[1];
		});		
	});
});

// Telechargement
casper.then(function(){
	casper.echo("### Téléchargement");
	chap = DEPART;
});

casper.repeat(ARRIVEE-DEPART+1, function(){
	casper.then(function(){
		if(VOLUME){
			casper.echo("## Volume " + chap);
		}
		else {
			casper.echo("## Chapitre " + chap);
		}
	});

	// Recuperer la page maximum du chapitre
	casper.wait(300, function(){
		casper.thenOpen("https://www.japscan.to/lecture-en-ligne/" + NOM + "/" + (VOLUME ? "volume-"+chap : chap) + "/" , function(){
			var xxx = casper.evaluate(function() {
				return document.querySelectorAll("option[data-img]");
			});
			PAGEMAX = xxx.length;
			if(VOLUME){
				casper.echo("# Page maximum du volume " + chap + " : " + PAGEMAX);
			}
			else {
				casper.echo("# Page maximum du chapitre " + chap + " : " + PAGEMAX);
			}
		});
	});

	casper.then(function(){
		for (var page = 1; page <= PAGEMAX; page++) {
			downImage("https://www.japscan.to/lecture-en-ligne/" + NOM + "/" + (VOLUME ? "volume-"+chap : chap) + "/" + page + ".html", chap);
		}
	});

	casper.then(function(){
		chap++;
	});
});

casper.run(function() {
    this.echo('### Fin').exit();
});


