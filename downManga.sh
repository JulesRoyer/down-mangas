#!/bin/bash
function help(){
	printf "\n *----- USAGE -----* "
	printf "\nArguments = -m nom_manga -d chap_depart -a chap_arrivee\n"
	printf "nom_manga du type : 'my-hero-academia', à trouver dans l'url du manga que vous lisez\n"
	printf 'Possibilite de telecharger des volumes avec --volume\n\n\n'
}

arechercher= # Nom du manga dans l'url
chap=0 # Chapitre/volume de depart
chapMax=0 # Chapitre/volume d'arrivee
volume=false # Flag de telechargement de chapitre ou de volume

# On parse les arguments
while [ "$#" -gt 0 ]
do
	case $1 in
		-h) help; exit 0;;
		-m) if [ "$2" ] 
			then 
				arechercher=$(echo $2 | tr '-' ' ' | tr -s ' ' | tr ' ' '-' | tr '[:upper:]' '[:lower:]');
				printf "/ On veut %s /" "$arechercher";
				shift 2;
			else
				shift;
			fi
			;;
		-d) if [ "$2" ] 
			then 
				chap=$2;
				printf "/ Chapitre de depart %d /" $chap;
				shift 2;
			else
				shift;
			fi
			;;
		-a) if [ "$2" ] 
			then 
				chapMax=$2;
				printf "/ Chapitre d'arrivee %d /" $chapMax;
				shift 2;
			else
				shift;
			fi
			;;
		--volume) volume=true;
			printf "/ Mode volume /";
			shift;;
		*) shift;;
	esac
done

printf "\n"

# Verification que le necessaire est bon
if [ "$arechercher" -a "$chap" -gt 0 ]
then
	if(("$chapMax" <= 0 || "$chapMax" < "$chap"))
	then
		chapMax=$chap;
	fi

	# "ou" à changer en fonction de là où vous voulez enregistrer les mangas (et leurs chapitres) [à partir de ce dossier]
	if(("$volume"))
	then
		casperjs --web-security=no download.js --nom="$arechercher" --depart="$chap" --arrivee="$chapMax" --ou="../Mangas" --volume
	else
		casperjs --web-security=no download.js --nom="$arechercher" --depart="$chap" --arrivee="$chapMax" --ou="../Mangas"
	fi
else
	printf "\n\nErreur : mauvais arguments (ou pas assez)\n\n"
	help
fi 