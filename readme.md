# Projet de téléchargement de mangas
## **ATTENTION**
**Patch pour contourner la protection CloudFlare, utilisation de CasperJS et PhantomJS**

## Utilisation
### Arguments
Peuvent être dans le désordre : ```-m "Nom_manga" -d chapitre_depart -a chapitre_arrivee --volume```

#### Obligatoires
* ```-m "Nom_manga"``` Nom_manga doit vraiment être entre " " ou ' '. Nom du manga comme appelé sur japscan ou dans l'url pour y accéder.

Exemple : ```www.japscan.to/lecture-en-ligne/my-hero-academia/1/```, le nom ici pourra être **my-hero-academia** ou **My Hero Academia** comme on peut le retrouver sur la page réservée au manga : 

![exemple](howto.png)

* ```-d chapitre_depart``` chapitre_depart est un nombre >0.

#### Optionnels
* ```-a chapitre_arrivee``` chapitre_arrivee est un nombre plus grand que chapitre_depart.
Si pas entré ou inférieur à chapitre_depart alors un seul chapitre telechargé : chapitre_depart.
* ```--volume``` permet de télécharger des volumes plutôt que des chapitres (rangement propre au site).

#### Aide
* ```-h``` vous affiche l'utilisation du script.
* Si mauvaise utilisation, l'aide vous est affichée.

### Exemple d'utilisation
```./downManga.sh -m 'my-hero-academia' -d 142 -a 144```

ou

```./downManga.sh -m 'My Hero Academia' -d 142 -a 144```

donneront le même résultat.

## Mais où se trouvent les chapitres que j'ai téléchargé ?
Une arborescence de dossiers dans le dossier où se trouve le script se créera de cette forme :
* nom-du-manga/
	* x/
	* y/
	* ...

Les dossiers de chapitre/volume (ici x ou y) sont des numéros et comportent les pages sous forme d'images.

## Prochainement 
**Le retour de la recherche et de la préselection**