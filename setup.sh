#!/bin/bash

printf "Début de l'installation"

########## Installation PhantomJS

printf "# Installation PhantomJS\n"

if [[ "$OSTYPE" == "linux-gnu" ]]; then
	cd phantomjs/linux
elif [[ "$OSTYPE" == "darwin"* ]]; then
	cd phantomjs/mac
fi

printf "Entrez votre mot de passe pour ajouter PhantomJS au PATH\n"
sudo ln -sf `pwd`/phantomjs /usr/local/bin/phantomjs

cd ../..

########## Installation CasperJS

printf "# Installation CasperJS\n"

git clone git://github.com/casperjs/casperjs.git

cd casperjs

printf "Entrez votre mot de passe pour ajouter CasperJS au PATH\n"
sudo ln -sf `pwd`/bin/casperjs /usr/local/bin/casperjs

cd ..

printf "Fin de l'installation\n"